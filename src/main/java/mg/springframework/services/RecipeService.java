package mg.springframework.services;

import mg.springframework.model.Recipe;

import java.util.Set;

/**
 * Created by Diego07 on 2017-11-11.
 */
public interface RecipeService {

    Set<Recipe> getRecipes();

    Recipe findById(Long l);

    Recipe saveRecipe(Recipe command);

    void deleteById(Long idToDelete);
}
