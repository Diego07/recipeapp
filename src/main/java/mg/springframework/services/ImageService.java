package mg.springframework.services;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Diego07 on 2017-11-12.
 */
public interface ImageService {

    void saveImageFile(Long recipeId, MultipartFile file);
}

