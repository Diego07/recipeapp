package mg.springframework.cotrollers;

import lombok.extern.slf4j.Slf4j;
import mg.springframework.services.RecipeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Diego07 on 2017-11-11.
 */
@Slf4j
@Controller
public class IndexController {

    private final RecipeService recipeService;

    public IndexController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @RequestMapping({"", "/", "/index"})
    public String getIndexPage(Model model) {

        log.debug("I'm in the Controller..");

        model.addAttribute("recipes", recipeService.getRecipes());

        return "index";
    }

}
