package mg.springframework.repositories;

import mg.springframework.model.Recipe;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Diego07 on 2017-11-11.
 */
public interface RecipeRepository extends CrudRepository<Recipe, Long> {


}
