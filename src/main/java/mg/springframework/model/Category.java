package mg.springframework.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Diego07 on 2017-11-11.
 */
@Data
@EqualsAndHashCode(exclude = {"recipes"})
@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;

    @ManyToMany(mappedBy = "category")
    private Set<Recipe> recipes;

}
