package mg.springframework.model;

/**
 * Created by Diego07 on 2017-11-11.
 */
public enum Difficulty {
    EASY, MODERATE, HARD
}
