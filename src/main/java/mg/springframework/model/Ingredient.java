package mg.springframework.model;

import lombok.*;
import org.springframework.context.annotation.Primary;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Diego07 on 2017-11-11.
 */
//@Data
//@EqualsAndHashCode(exclude = {"recipe"} )
//@Entity
//public class Ingredient {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    private String description;
//    private BigDecimal amount;
//
//    @OneToOne(fetch = FetchType.EAGER)
//    private UnitOfMeasure uom;
//
//    @ManyToOne
//    private Recipe recipe;
//
//    public Ingredient(String description, BigDecimal amount, UnitOfMeasure uom) {
//        this.description = description;
//        this.amount = amount;
//        this.uom = uom;
//    }
//
//
//
//}

@Data
@EqualsAndHashCode(exclude = {"recipe"})
@Entity
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private BigDecimal amount;

    @OneToOne(fetch = FetchType.EAGER)
    private UnitOfMeasure uom;

    @ManyToOne
    private Recipe recipe;

    public Ingredient() {
    }

    public Ingredient(String description, BigDecimal amount, UnitOfMeasure uom) {
        this.description = description;
        this.amount = amount;
        this.uom = uom;
    }

    public Ingredient(String description, BigDecimal amount, UnitOfMeasure uom, Recipe recipe) {
        this.description = description;
        this.amount = amount;
        this.uom = uom;
        this.recipe = recipe;
    }

}
